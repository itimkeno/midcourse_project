#pragma once

#include "ui_midcourse_project.h"
#include <QtWidgets/QWidget>
#include <QIntValidator>
#include <memory>

class MyClass : public QObject
{
    Q_OBJECT

public:
    MyClass(int field1, int field2);
    QString info_output() const;
    bool process_data();
    void set_data(int field1, int field2);

signals:
    void update_result_output(const QString& result, const bool is_error = false);

protected:
    int field1;
    int field2;
};

class MySubClass : public MyClass
{
    Q_OBJECT

public:
    MySubClass(int field1, int field2, double field3);
    QString info_output() const;
    bool process_data();
    void set_data(int field1, int field2, double field3);

signals:
    void update_result_output(const QString& result, const bool is_error = false);

private:
    double field3;
};

class midcourse_project : public QWidget
{
    Q_OBJECT

public:
    midcourse_project(QWidget* parent = nullptr);
    ~midcourse_project();

private slots:
    void on_button_parrent_info_clicked();
    void on_button_child_info_clicked();
    void on_button_parent_calculate_clicked();
    void on_button_child_calculate_clicked();
    void set_parrent_error_style_if_needed(const QString& result, const bool is_error);
    void set_child_error_style_if_needed(const QString& result, const bool is_error);

private:
    Ui::midcourse_projectClass ui;
    QLineEdit* lineEdit_parent_x;
    QLineEdit* lineEdit_parent_y;
    QLineEdit* lineEdit_child_z;
    QPushButton* button_parrent_info;
    QPushButton* button_child_info;
    QPushButton* button_parent_calculate;
    QPushButton* button_child_calculate;
    QLabel* label_info_output;
    QLabel* label_child_result;
    QLabel* label_parent_result;

    std::unique_ptr<MyClass> parrent_instance;
    std::unique_ptr<MySubClass> child_instance;

    std::unique_ptr<QValidator> validator_int;
    std::unique_ptr<QValidator> validator_double;
};

