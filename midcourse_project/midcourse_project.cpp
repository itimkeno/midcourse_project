#include "midcourse_project.h"

midcourse_project::midcourse_project(QWidget* parent)
    : QWidget(parent)
{
    // defualt init
    parrent_instance = std::make_unique<MyClass>(0, 0);
    child_instance = std::make_unique<MySubClass>(0, 0, 0);

    // set ui
    ui.setupUi(this);
    lineEdit_parent_x = ui.lineEdit_parent_x;
    lineEdit_parent_y = ui.lineEdit_parent_y;
    lineEdit_child_z = ui.lineEdit_child_z;
    button_parrent_info = ui.button_parrent_info;
    button_child_info = ui.button_child_info;
    button_parent_calculate = ui.button_parent_calculate;
    button_child_calculate = ui.button_child_calculate;
    label_info_output = ui.label_info_output;
    label_child_result = ui.label_child_result;
    label_parent_result = ui.label_parent_result;

    // set custom font
    QFont font("Custom Name");
    font.setStyleHint(QFont::OldEnglish);
    font.setBold(false);
    font.setItalic(false);
    font.setWeight(QFont::Weight::Thin);
    this->setFont(font);

    // set colors
    ui.frame_main->setStyleSheet("{ color: #222831; }");
    ui.frame_main->setStyleSheet("QFrame { background-color: #76ABAE; }");
    ui.child->setStyleSheet("QFrame { background-color: #76ABAE; }");
    ui.parent->setStyleSheet("QFrame { background-color: #76ABAE; }");
    ui.button_child_calculate->setStyleSheet("QPushButton { background-color: #294D61; color: #EEEEEE; }");
    ui.button_parent_calculate->setStyleSheet("QPushButton { background-color: #294D61; color: #EEEEEE; }");
    ui.button_child_info->setStyleSheet("QPushButton { background-color: #294D61; color: #EEEEEE; }");
    ui.button_parrent_info->setStyleSheet("QPushButton { background-color: #294D61; color: #EEEEEE; }");
    ui.lineEdit_parent_x->setStyleSheet("QLineEdit { background-color: #EEEEEE; }");
    ui.lineEdit_parent_y->setStyleSheet("QLineEdit { background-color: #EEEEEE; }");
    ui.lineEdit_child_z->setStyleSheet("QLineEdit { background-color: #EEEEEE; }");

    // set line_edit validators
    const int max_ui_int = 100000;
    const int min_ui_int = -100000;
    const double max_ui_double = 100000.0;
    const double min_ui_double = -100000.0;
    validator_int = std::make_unique<QIntValidator>(min_ui_int, max_ui_int, this);
    validator_double = std::make_unique<QDoubleValidator>(min_ui_double, max_ui_double, 3, this);
    lineEdit_parent_x->setValidator(validator_int.get());
    lineEdit_parent_y->setValidator(validator_int.get());
    lineEdit_child_z->setValidator(validator_double.get());

    // signal / slot connections
    connect(button_parrent_info, SIGNAL(clicked()), this, SLOT(on_button_parrent_info_clicked()));
    connect(button_child_info, SIGNAL(clicked()), this, SLOT(on_button_child_info_clicked()));
    connect(button_parent_calculate, SIGNAL(clicked()), this, SLOT(on_button_parent_calculate_clicked()));
    connect(button_child_calculate, SIGNAL(clicked()), this, SLOT(on_button_child_calculate_clicked()));
    connect(parrent_instance.get(), &MyClass::update_result_output, this, &midcourse_project::set_parrent_error_style_if_needed);
    connect(child_instance.get(), &MySubClass::update_result_output, this, &midcourse_project::set_child_error_style_if_needed);
}

midcourse_project::~midcourse_project() {}

MyClass::MyClass(int field1, int field2)
{
    this->field1 = field1;
    this->field2 = field2;
}

QString MyClass::info_output() const
{
    return QString::fromUtf8(u8"Состояние объекта родительского класса:\n x = %1 ; y =: %2").arg(field1).arg(field2);
}

bool MyClass::process_data()
{
    if (0 == field2)
    {
        emit update_result_output(QString::fromUtf8(u8"Комплюктер не умеет делить на ноль!"), true);
        return true;
    }

    int result = field1 / field2;
    qDebug() << "MyClass::process_data: " << result;
    emit update_result_output(QString::fromUtf8(u8"Результат вычисления метода родительского класса: %1").arg(result));

    return false;
}

void MyClass::set_data(int field1, int field2) {
    this->field1 = field1;
    this->field2 = field2;
}

MySubClass::MySubClass(int field1, int field2, double field3) : MyClass(field1, field2)
{
    this->field3 = field3;
}

QString MySubClass::info_output() const
{
    return QString::fromUtf8(u8"Состояние объекта дочернего класса:\n x = %1; y = %2; z = %3").arg(field1).arg(field2).arg(field3);
}

bool MySubClass::process_data()
{
    if (0.0 == field3)
    {
        emit update_result_output(QString::fromUtf8(u8"Комплюктер не умеет делить на ноль!"), true);
        return true;
    }

    double result = (static_cast<double>(field1) / field3) + (static_cast<double>(field2) / field3);
    qDebug() << "MySubClass::process_data: " << result;
    emit update_result_output(QString::fromUtf8(u8"Результат вычисления метода дочернего класса: %1").arg(result));

    return false;
}

void MySubClass::set_data(int field1, int field2, double field3) {
    this->field1 = field1;
    this->field2 = field2;
    this->field3 = field3;
}

void midcourse_project::on_button_parrent_info_clicked()
{
    label_info_output->setText(parrent_instance->info_output());
}

void midcourse_project::on_button_child_info_clicked()
{
    label_info_output->setText(child_instance->info_output());
}

void midcourse_project::on_button_parent_calculate_clicked()
{
    parrent_instance->set_data(lineEdit_parent_x->text().toInt(),
                               lineEdit_parent_y->text().toInt());
    parrent_instance->process_data();
}

void midcourse_project::on_button_child_calculate_clicked()
{
    child_instance->set_data(lineEdit_parent_x->text().toInt(),
                             lineEdit_parent_y->text().toInt(),
                             lineEdit_child_z->text().toDouble());
    child_instance->process_data();
}

void midcourse_project::set_parrent_error_style_if_needed(const QString& result, const bool is_error)
{
    if (is_error)
        label_parent_result->setStyleSheet("QLabel { color : #A63446; }");
    else
        label_parent_result->setStyleSheet("QLabel { color : #222831; }");

    label_parent_result->setText(result);
}

void midcourse_project::set_child_error_style_if_needed(const QString& result, const bool is_error)
{
    if (is_error)
        label_child_result->setStyleSheet("QLabel { color : #A63446; }");
    else
        label_child_result->setStyleSheet("QLabel { color : #222831; }");

    label_child_result->setText(result);
}
