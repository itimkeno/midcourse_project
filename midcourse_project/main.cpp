#include "midcourse_project.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    midcourse_project w;
    w.setWindowIcon(QIcon(":/midcourse_project/main_ico"));
    w.show();
    return a.exec();
}
